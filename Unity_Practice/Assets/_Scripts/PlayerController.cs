﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    
    Rigidbody rb;
    public float Speed;

    int count = 0;

    public Text countText;
    public Text WinText;

    public GameObject NorthWall;
    public GameObject SouthWall;
    public GameObject WestWall;
    public GameObject EastWall;
    public GameObject Straight1;
    public GameObject Straight2;
    public GameObject Game2;
    public GameObject Game3;
    public GameObject Game4;
    public GameObject Straight3;

    bool dead = false;


    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update () {

        if (dead == false && transform.position.y <= -5)
        {
            dead = true;
            GetComponent<MeshRenderer>().enabled = false;
            Invoke("RestartLevel", 2f);

        }
		
	}

    void RestartLevel()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }


    private void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * Time.deltaTime * Speed);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup")) ;

        count += 1;

        SetCountText();
        if (count >= 39)
        {

            Game4.SetActive(true);
            Straight3.SetActive(true);

        }

        if (count >= 13)
        {

            Destroy(NorthWall);
            Straight1.SetActive(true);
            Straight2.SetActive(true);
            Game2.SetActive(true);
            Game3.SetActive(true);

        }
        if (count >= 10)
        {

            Destroy(SouthWall);

        }
        if (count >= 7)
        {

            Destroy(EastWall);

        }
        if (count >= 3)
        {

            Destroy(WestWall);

        }
        //if (count >= 13)
        //{

        //    wall.SetActive(false);

        //}

        Destroy(other.gameObject);

    }

    void SetCountText()
    {

        countText.text = "Count: " + count.ToString();

        if (count >= 52)
        {

            WinText.text = "You Win!";
        }

    }


}
